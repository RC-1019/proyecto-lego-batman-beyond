using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Coin : MonoBehaviour
{
    public AudioClip sonido;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Batman")
        {
            AudioSource.PlayClipAtPoint(sonido, gameObject.transform.position);
            if(sonido == true)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
