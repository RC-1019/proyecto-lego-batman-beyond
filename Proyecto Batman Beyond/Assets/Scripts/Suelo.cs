using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Suelo : MonoBehaviour
{
    public string escena;

    private void OnTriggerEnter(Collider other)
    {
        SceneManager.LoadScene(escena);
    }
}
