using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Puntaje : MonoBehaviour
{
    private int score;
    public TextMeshProUGUI scoreText;
    public string scene;

    private void Start()
    {
        score = 0;
        scoreText.text = "Score: " + score;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Coin")
        {
            score++;
            scoreText.text = "Score: " + score;
        }
    }

    private void Update()
    {
        if (score == 8)
        {
            SceneManager.LoadScene(scene);
        }
    }
}
