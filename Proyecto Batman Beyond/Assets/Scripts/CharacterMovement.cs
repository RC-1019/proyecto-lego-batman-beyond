using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CharacterMovement : MonoBehaviour
{
    public Animator anim;
    
    [Header("Movimiento")]
    [Range(0, 10)]
    public float speed;
    public float speedRotation;

    Rigidbody rb;

    bool jumpFloor;
    bool jumpTrue = false;
    public float jumpForce = 5f;
    public AudioSource salto;

    [Header("Checkpoint")]
    public Respawn checkpoint;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Lava")
        {
            transform.position = checkpoint.spawnPoint;
        }

        if (other.transform.tag == "Checkpoint")
        {
            checkpoint = other.GetComponent<Respawn>();
        }
    }

    private void Update()
    {
        Vector3 moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
        anim.SetFloat("Speed", moveDirection.z);
        transform.Translate((moveDirection * speed) * Time.deltaTime);
        transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal") * speedRotation, 0));

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            speed = 15f;
            anim.SetBool("Correr", true);
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            speed = 6f;
            anim.SetBool("Correr", false);
        }
        

        Vector3 floor = transform.TransformDirection(Vector3.down);

        if(Physics.Raycast(transform.position, floor, 0.5f))
        {
            jumpFloor = true;
        }
        else
        {
            jumpFloor = false;
        }
        jumpTrue = Input.GetButtonDown("Jump");

        if(jumpTrue && jumpFloor)
        {
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            anim.SetBool("Salto", true);
            salto.Play();

        }
        if(!jumpTrue && jumpFloor)
        {
            anim.SetBool("Salto", false);
        }
    }
}
